package com.interviewcake.computefib;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by elliosim on 17/05/2017.
 */
public class SolutionTest {

    Solution solution = new Solution();
    //        [0,1,1,2,3]

    @Test
    public void testFirstFibIsZero() throws Exception {
        assertEquals(0, solution.fib(0));
    }

    @Test
    public void testSecondFibIsOne() throws Exception {
        assertEquals(1, solution.fib(1));
    }

    @Test
    public void testThirdFibIsOne() throws Exception {
        assertEquals(1, solution.fib(2));
    }

    @Test
    public void testFourthFib() throws Exception {
        assertEquals(3, solution.fib(4));
    }

}