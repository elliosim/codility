package com.interviewcake.computefib;

public class Solution {


    /*
    Write a function fib() that a takes an integer n and returns the nth fibonacci ↴ number.
    Let's say our fibonacci series is 0-indexed and starts with 0. So:

    fib(0); // => 0
    fib(1); // => 1
    fib(2); // => 1
    fib(3); // => 2
    fib(4); // => 3
    ...

     */

    public int fib(int i) {
        if(i < 2) {
            return i;
        }

        int sum = 0;
        int prev = 0;
        int next = 1;

        for (int j = 1; j < i; j++) {
            sum = prev + next;
            prev = next;
            next = sum;
        }
        return sum;
    }
}
