package com.codility.odd;

// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class OddOccurrencesInArray {

    public int solution(int[] A) {

        int num = A[0];
        for(int i = 1; i < A.length; i++) {
            num ^= A[i];
        }
        return num;
    }
}