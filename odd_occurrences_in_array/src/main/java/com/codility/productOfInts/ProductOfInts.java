package com.codility.productOfInts;

public class ProductOfInts {
    public int[] getProductsOfAllIntsExceptAtIndex(int[] array) {

        if(array.length < 2) {
            throw new IllegalArgumentException("Need more than 1 element!");
        }

        int[] products = new int[array.length];

        for(int i = 0; i < array.length; i++){

            int product = 1;

            for(int j = 0; j < array.length; j++){
                if(i != j) {
                    product = product * array[j];
                }
            }
            products[i] = product;
        }
        return products;
    }

    public int[] getProductsOfIntsBeforeIndex(int[] array) {
        int[] productOfAllIntsBeforeIndex = new int[array.length];
        int productsSoFar = 1;

        for(int i = 0; i < array.length; i++) {
            productOfAllIntsBeforeIndex[i] = productsSoFar;
            productsSoFar *= array[i];
        }
        return productOfAllIntsBeforeIndex;
    }

    public int[] getProductsOfIntsAfterIndex(int[] array) {
        int[] productOfAllIntsAfterIndex = new int[array.length];
        int productsSoFar = 1;

        for(int i = array.length-1; i >= 0; i--) {
            productOfAllIntsAfterIndex[i] = productsSoFar;
            productsSoFar *= array[i];
        }
        return productOfAllIntsAfterIndex;
    }
}
