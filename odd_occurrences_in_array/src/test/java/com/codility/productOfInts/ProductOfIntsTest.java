package com.codility.productOfInts;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by simonelliott on 28/04/2017.
 */
public class ProductOfIntsTest {

    private ProductOfInts ints = new ProductOfInts();

    @Test
    public void testProductOfInts() throws Exception {
        int[] array = new int[] {1,7,3,4};
        assertArrayEquals(new int[]{84,12,28,21}, ints.getProductsOfAllIntsExceptAtIndex(array));
    }

    @Test
    public void testProductOfIntsWithZeros() throws Exception {
        int[] array = new int[] {1,7,0,4};
        assertArrayEquals(new int[]{0,0,28,0}, ints.getProductsOfAllIntsExceptAtIndex(array));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testProductOfIntsWith1Element() throws Exception {
        int[] array = new int[] {8};
        ints.getProductsOfAllIntsExceptAtIndex(array);
    }

    @Test
    public void testProductOfIntsLonger() throws Exception {
        int[] array = new int[] {1,2,6,5,9};
        assertArrayEquals(new int[]{540,270,90,108,60}, ints.getProductsOfAllIntsExceptAtIndex(array));
    }

    @Test
    public void testProductOfIntsBeforeIndex() throws Exception {
        int[] array = new int[] {3,1,2,5,6,4};
        /*
        [1, 3, 3*1, 3*1*2, 3*1*2*5,3*1*2*5*6]
         */
        assertArrayEquals(new int[]{1,3,3,6,30,180}, ints.getProductsOfIntsBeforeIndex(array));
    }

    @Ignore
    @Test
    public void testProductOfIntsAfterIndex() throws Exception {
        int[] array = new int[] {3,1,2,5,6,4};
        /*
        [1, 4, 4*6, 4*6*5, 4*6*5*2, 4*6*5*2*1]
         */
        assertArrayEquals(new int[]{1,4,24,48,48,180}, ints.getProductsOfIntsAfterIndex(array));
    }
}