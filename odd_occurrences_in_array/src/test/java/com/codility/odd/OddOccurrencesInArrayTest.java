package com.codility.odd;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by simonelliott on 06/04/2017.
 */
public class OddOccurrencesInArrayTest {

    OddOccurrencesInArray occurrencesInArray = new OddOccurrencesInArray();

    @Test
    public void testUnpairedValueIs7() throws Exception {
        int[] array = {9,3,9,3,9,7,9};
        assertEquals(7, occurrencesInArray.solution(array));
    }

    @Test
    public void testUnpairedValueIs6() throws Exception {
        int[] array = {2,7,2,7,6,2,5,2,5};
        assertEquals(6, occurrencesInArray.solution(array));
    }

    @Test
    public void testUnpairedValueIs8() throws Exception {
        int[] array = {9,3,9,3,9,8,9};
        assertEquals(8, occurrencesInArray.solution(array));
    }

    @Test
    public void testMediumRandom() {
        int arraySize = 2001;
        int[] array = new int[arraySize];
        array[0] = 256;
        for(int i = 1; i < arraySize; i++) {
            if(i % 2 == 0) {
                array[i] = 54323453;
            } else {
                array[i] = 64264111;
            }
        }
        assertEquals(256, occurrencesInArray.solution(array));
    }

}