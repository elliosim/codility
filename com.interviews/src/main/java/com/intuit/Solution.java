package com.intuit;

import java.io.*;
import java.util.*;

import org.junit.*;
import org.junit.runner.*;
import static org.junit.Assert.*;

/*
 * Implement a data structure, ToyBlob, backed by plain java arrays(s), with the following methods:
* - size which returns the number of elements in its collection
* - add which adds an element to its middle when the ToyBlob's size is even or the end when odd.
 * - remove which removes and returns the element at the end of the collection
 * - toString which pretty-prints the elements
 */
public class Solution {

    ToyBlob<String> toyBlob = new ToyBlob<>();

    @Test
    public void testArraySize() {
        assertEquals(0, toyBlob.size());
    }

    @Test
    public void testAddElementToEnd() {
        toyBlob.add("foo");
        assertEquals(1, toyBlob.size());
        assertEquals("foo", toyBlob.get(toyBlob.size()-1));
    }

    @Test
    public void testAddElementToEndOfLargerArray() {
        createToyBlobWithElements(11);
        toyBlob.add("test");
        assertEquals("test", toyBlob.get(11));
    }

    @Test
    public void testAddElementToMiddle() {
        createToyBlobWithElements(20);
        assertEquals(20, toyBlob.size());

        toyBlob.add("baz");
        assertEquals("baz", toyBlob.get(10));
    }

    @Test
    public void testRemoveElement() {
        createToyBlobWithElements(9);
        toyBlob.add("removed");
        assertEquals(10, toyBlob.size());
        assertEquals("removed", toyBlob.remove());
        assertEquals(9, toyBlob.size());
    }

    @Test
    public void testToString() {
        createToyBlobWithElements(5);
        assertEquals("ToyBlob[foo0, foo2, foo4, foo1, foo3]", toyBlob.toString());
        System.out.print(toyBlob.toString());
    }

    private void createToyBlobWithElements(int size) {
        for(int i = 0; i < size; i++) {
            toyBlob.add("foo"+i);
        }
    }

    public static void main(String[] args) {
        JUnitCore.main("Solution");
        //this method will be executed from "Run" above so you may write code here to verify your solution.  You can also execute Unit Tests (click the icon to the left of "Java" at the top of this page to see CoderPad's instructions)

        //System.out.println();
    }
}

//create any additional classes here
class ToyBlob<E> {

    // this took longer as I tried to implement dynamic array resizing
    private Object[] array = new Object[50];
    private int size;

    public int size() {
        return size;
    }

    public void add(E element) {
        if(size != 0 && size % 2 == 0) {

            int index = size / 2;
            System.arraycopy(array, index, array, index + 1, size - index);
            this.array[index] = element;
            size++;

        } else {
            this.array[size++] = element;
        }
    }

    public E remove() {
        E removedElement = (E)this.array[size-1];
        this.array[size--] = null;
        return removedElement;
    }

    public E get(int elementPosition) {
        return (E)this.array[elementPosition];
    }

    @Override
    public String toString() {

        StringBuilder str = new StringBuilder();
        str.append("ToyBlob[");

        for(int i = 0; i < size; i++) {
            str.append(String.valueOf(this.array[i]));
            if(i < size - 1) {
                str.append(", ");
            }
        }
        str.append("]");

        return str.toString();
    }
}