package com.codility.apple_stocks;

public class Stocks {


    public int getMaxProfit(int[] stockPricesYesterday) {

        int lowestStockPosition = getLowestStockIndex(stockPricesYesterday);
        int highestStockAfterLowestPosition = getHighestStockAfterLowestPosition(lowestStockPosition, stockPricesYesterday);

        return highestStockAfterLowestPosition - stockPricesYesterday[lowestStockPosition];
    }

    public int getLowestStockIndex(int[] stockPricesYesterday) {
        int lowestStock = stockPricesYesterday[0];

        int index = -1;
        for(int i = 1; i < stockPricesYesterday.length; i++) {
            if(lowestStock > stockPricesYesterday[i]) {
                lowestStock = stockPricesYesterday[i];
                index = i;
            }
        }
        return index;
    }

    public int getHighestStockAfterLowestPosition(int position, int[] stockPricesYesterday) {
        int highestStock = stockPricesYesterday[position];

        for(int i = position; i < stockPricesYesterday.length; i++) {
            if(highestStock < stockPricesYesterday[i]) {
                highestStock = stockPricesYesterday[i];
            }
        }
        return highestStock;
    }
// returns 6 (buying for $5 and selling for $11)
}
