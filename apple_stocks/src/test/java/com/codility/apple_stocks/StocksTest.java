package com.codility.apple_stocks;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StocksTest {

    Stocks stocks = new Stocks();

    @Test
    public void testGetLowestStockIndex() throws Exception {
        int[] stockPricesYesterday = new int[]{10, 7, 5, 8, 11, 9};
        assertEquals(stocks.getLowestStockIndex(stockPricesYesterday),2);
    }

    @Test
    public void testGetHighestStock() throws Exception {
        int[] stockPricesYesterday = new int[]{10, 7, 5, 8, 11, 9};

        int position = 2;
        assertEquals(stocks.getHighestStockAfterLowestPosition(position, stockPricesYesterday),11);
    }

    @Test
    public void testGetMaxProfit(){
        int[] stockPricesYesterday = new int[]{10, 7, 5, 8, 11, 9};
        assertEquals(stocks.getMaxProfit(stockPricesYesterday), 6);
    }

    @Test
    public void testStockPricesFalls() throws Exception {
        int[] stockPricesYesterday = new int[]{15, 13, 11, 10, 9, 7};
        assertEquals(stocks.getMaxProfit(stockPricesYesterday), 0);
    }
}