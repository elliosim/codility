import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ProductOfNumbersTest {

    ProductOfNumbers pont = new ProductOfNumbers();

    @Test
    public void testGetProductsOfAllIntsExceptAtIndex() {
        int arr[] = {1, 7, 3, 4};
        assertArrayEquals(new int[]{84, 12, 28, 21}, pont.testGetProductsOfAllIntsExceptAtIndex(arr));
    }
}