import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * A small frog wants to get to the other side of the road.
 * The frog is currently located at position X and wants to get to a position greater than or equal to Y.
 * The small frog always jumps a fixed distance, D.
 *
 * Count the minimal number of jumps that the small frog must perform to reach its target.
 *
 * Write a function:
 *
 * class Solution { public int solution(int X, int Y, int D); }
 *
 * that, given three integers X, Y and D, returns the minimal number of jumps from
 * position X to a position equal to or greater than Y.
 *
 * For example, given:
 *
 *   X = 10
 *   Y = 85
 *   D = 30
 * the function should return 3, because the frog will be positioned as follows:
 *
 * after the first jump, at position 10 + 30 = 40
 * after the second jump, at position 10 + 30 + 30 = 70
 * after the third jump, at position 10 + 30 + 30 + 30 = 100
 * Write an efficient algorithm for the following assumptions:
 *
 * X, Y and D are integers within the range [1..1,000,000,000];
 * X ≤ Y.
 *
 */

public class SolutionTest {

    @Test
    public void testFrogJumps3Times() {
        int x = 10, y = 85, d = 30;
        Solution solution = new Solution();
        assertEquals(3, solution.solution(x, y, d));
    }

    @Test
    public void testFrogJumps4Times() {
        int x = 32, y = 54, d = 7;
        Solution solution = new Solution();
        assertEquals(4, solution.solution(x, y, d));
    }

    @Test
    public void testFrogNoJumpsNeeded() {
        int x = 5, y = 5, d = 1;
        Solution solution = new Solution();
        assertEquals(0, solution.solution(x, y, d));
    }

    @Test(timeout=5)
    public void testLargeNumberRunsWithinTimelimit() {
        int x = 3, y = 999111321, d = 7;
        Solution solution = new Solution();
        assertEquals(142730189, solution.solution(x, y, d));
    }
}