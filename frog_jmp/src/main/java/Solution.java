import java.math.BigDecimal;
import java.math.RoundingMode;

public class Solution {

    public int solution(int X, int Y, int D) {
        return new BigDecimal(Y - X)
                .divide(BigDecimal.valueOf(D), RoundingMode.UP).intValue();
    }

}
