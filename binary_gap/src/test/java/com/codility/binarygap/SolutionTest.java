package com.codility.binarygap;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void testStandard() {
        assertEquals(2, solution.solution(9));
    }

    @Test
    public void testStandard2() {
        assertEquals(4, solution.solution(529));
    }

    @Test
    public void testStandard3() {
        assertEquals(1, solution.solution(20));
    }

    @Test
    public void testStandard4() {
        assertEquals(0, solution.solution(15));
    }

    @Test
    public void testStandard5() {
        assertEquals(5, solution.solution(1041));
    }

    @Test
    public void testStandard6() {
        assertEquals(3, solution.solution(561892));
    }


}
