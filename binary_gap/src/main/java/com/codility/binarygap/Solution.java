package com.codility.binarygap;
// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution {
    public int solution(int N) {
        String binaryString = Integer.toBinaryString(N);

        int highestGap = 0;
        int size = 0;

        for(String str : binaryString.split("")) {

            if(str.equals("0")) {
                size++;
            } else if(str.equals("1")) {
                if(size >= highestGap) {
                    highestGap = size;
                }
                size = 0;
            }
        }
        return highestGap;
    }
}