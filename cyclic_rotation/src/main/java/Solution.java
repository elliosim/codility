
/*
    A zero-indexed array A consisting of N integers is given.
    Rotation of the array means that each element is shifted right by one index,
    and the last element of the array is also moved to the first place.

    For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7].
    The goal is to rotate array A K times; that is, each element of A will be shifted to the right by K indexes.

    Write a function:

    class Solution { public int[] solution(int[] A, int K); }

    that, given a zero-indexed array A consisting of N integers and an integer K, returns the array A rotated K times.

    For example, given array A = [3, 8, 9, 7, 6] and K = 3, the function should return [9, 7, 6, 3, 8].

    Assume that:

    N and K are integers within the range [0..100];
    each element of array A is an integer within the range [−1,000..1,000].
    In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.
 */
public class Solution {

    // 1) array A is [3, 8, 9, 7, 6]

    // 2) rotate A by K times, K = 3

    // 3) create new array of same length as A

    // if rotating by the same length then element positions effectively do not move
    // divide K by length and get the remainder

    // 4) get A[index] and store in new array at position out[index + K]

    // 5) if index + K is larger than the array length - 1, then set out[index + K - length]

    // 7) repeat from 4 for each index in array

    public int[] solution(int[] A, int K) {

        if(A.length < 2) {
            return A;
        }

        int[] out = new int[A.length];

        K = K % A.length;

        for (int i = 0; i < A.length; i++) {
            if(i + K > A.length - 1) {
                out[i + K - A.length] = A[i];
            } else {
                out[i + K] = A[i];
            }
        }
        return out;
    }
}
