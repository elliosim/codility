import org.junit.Test;

import static org.junit.Assert.*;


public class SolutionTest {

    private Solution solution = new Solution();

    @Test
    public void testCyclicArray() throws Exception {
        int[] A = {3, 8, 9, 7, 6};
        int K = 1;
        int[] expected = {6, 3, 8, 9, 7};
        assertArrayEquals(expected, solution.solution(A, K));
    }

    @Test
    public void testCyclicArray2() throws Exception {
        int[] A = {3, 8, 9, 7, 6};
        int K = 3;
        int[] expected = {9, 7, 6, 3, 8};
        assertArrayEquals(expected, solution.solution(A, K));
    }

    @Test
    public void testSingleItem() throws Exception {
        int[] A = {1000};
        int K = 5;
        int[] expected = {1000};
        assertArrayEquals(expected, solution.solution(A, K));
    }

    @Test
    public void testSmall2() throws Exception {
        int[] A = {1, 1, 2, 3, 5};
        int K = 42;
        int[] expected = {3, 5, 1, 1, 2};
        assertArrayEquals(expected, solution.solution(A, K));
    }
}















