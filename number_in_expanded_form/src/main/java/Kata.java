/*

Write Number in Expanded Form
You will be given a number and you will need to return it as a string in Expanded Form. For example:

Kata.expandedForm(12); # Should return "10 + 2"
Kata.expandedForm(42); # Should return "40 + 2"
Kata.expandedForm(70304); # Should return "70000 + 300 + 4"
NOTE: All numbers will be whole numbers greater than 0.

 */
public class Kata {
    public static String expandedForm(int num) {
        StringBuilder sb = new StringBuilder();

        String[] arr = String.valueOf(num).split("");
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
            if(i == arr.length-1) {
                break;
            }
            if(Integer.parseInt(arr[i]) < 0) {
                continue;
            }
            for(int ii = arr.length-1; ii > 0; ii--) {
                sb.append("0");
            }
            sb.append(" + ");
        }
        return sb.toString();
    }
}
/*
70304
convert to string
split digits to arr = 7, 0, 3, 0, 4
loop array to append 0000, 00

 */