import junit.framework.TestCase;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runners.JUnit4;

// TODO failing

public class SolutionTest {
    @Test
    public void testExpand12() {
        assertEquals("10 + 2", Kata.expandedForm(12));
    }
    @Test
    public void testExpand42() {
        assertEquals("40 + 2", Kata.expandedForm(42));
    }
    @Test
    public void testExpand70304() {
        assertEquals("70000 + 300 + 4", Kata.expandedForm(70304));
    }
}