import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    @Test
    public void testMinimalDifference() {
        int A[] = {3,1,2,4,3};
        Solution solution = new Solution();
        assertEquals(1, solution.solution(A));
    }
}