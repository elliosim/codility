import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void testOrderedPosNums() {
        assertEquals(4, solution.solution(new int[] {1,2,3}));
    }

    @Test
    public void testUnorderedPosNums() {
        assertEquals(5, solution.solution(new int[] {1, 3, 6, 4, 1, 2}));
    }

    @Test
    public void testNegNums() {
        assertEquals(1, solution.solution(new int[] {-1, -3}));
    }

}