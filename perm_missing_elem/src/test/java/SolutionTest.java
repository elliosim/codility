import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution = new Solution();

    @Test
    public void testSmallArray() {
        int[] A = {2,3,1,5};
        assertEquals(4, solution.solution(A));
    }

    @Test
    public void testLargeArray() {
        int[] A = {2,3,1,5,4,20,15,16,18,6,9,10,12,19,7,8,11,14,17};
        assertEquals(13, solution.solution(A));
    }

    @Test
    public void testLargerArray() {
        int[] A = {2,3,1,5,4,20,15,16,18,6,13,9,10,12,19,7,8,11,14,17,21,22,23,24,25,26,27,28,29,30,32,33,34,35,36,37};
        assertEquals(31, solution.solution(A));
    }
}